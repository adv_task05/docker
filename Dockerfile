FROM ubuntu:20.04
RUN \
  apt update && \
  apt -y upgrade && \
  apt install -y make && \
  apt install -y git && \
  apt install -y build-essential && \
  git clone https://github.com/google/re2.git && \
  git clone https://github.com/sass/libsass.git && \
  cd re2 && \
  make clean && \
  make && \
  cd ../libsass && \
  make clean && \
  make && \
  cd ..
  
